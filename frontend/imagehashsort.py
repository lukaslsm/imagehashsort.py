#!/usr/bin/env python3
"""
MIT License

Copyright (c) 2020-2022 Lukas Kirschner

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

ImageHashSort - A Python Script to discard images that are already in a huge database of images, ignoring metadata.
"""
import argparse
import shutil
import sys
from collections import deque
from pathlib import Path
from typing import NoReturn, Any, Optional

from send2trash import send2trash

from imagehashsort import ImageDatabase, JSONImageDatabase, known_extensions, DelayedKeyboardInterrupt, perceptual_hash

autosave_interval: int = 1000  # Autosave database after x files


def is_picture(file: Path) -> bool:
    name: str = str(file.name)
    extension: str = name.split('.')[-1].casefold()
    return extension in known_extensions


def new_filename(original_name: Path, length_limit: int, imghash: str) -> Path:
    name: str = str(original_name.name)
    extension: str = name.split('.')[-1].casefold()
    if 0 < length_limit < len(imghash):
        imghash = imghash[:length_limit]
    ret: Path = original_name.parent / f"{imghash}.{extension}"
    return ret


def sanitize_path(path: Path) -> Path:
    parts = list()
    for part in path.parent.parts:
        parts.append(Path(str(part).rstrip(".")))
    return Path(*parts) / path.name


def delete(file: Path) -> NoReturn:
    """
    Move a file to the system trash.
    :param file: File to delete
    :return: None
    """
    try:
        send2trash(file)
    except OSError as e:  # See https://www2.hs-fulda.de/~klingebiel/c-stdlib/sys.errno.h.htm
        if e.errno == 36:
            print(f"Filename too long for recycle bin: {file}. This file will be permanently deleted.")
            file.unlink()


def process_image(image: Path,
                  library: ImageDatabase,
                  args: Any,
                  source_dir: Path,
                  dest_dir: Path,
                  duplicate_dir: Optional[Path]
                  ) -> bool:
    """
    Process an image and return True, if the database has been changed.
    :param dest_dir: Destination directory
    :param source_dir: Source Directory
    :param args: Arguments, as parsed from the Command-Line (read-only)
    :param library: Library
    :param known_hashes: Known Hashes
    :param known_filenames: Known Filenames
    :param image: Image to process
    :return: True if the database has been changed
    """
    assert image.is_file()
    with DelayedKeyboardInterrupt():
        if library.image_in_filenames(image):
            print(f"{image} was found in known file names and is therefore skipped.")
            return False
        try:
            imhash = perceptual_hash(image)
        except (IOError, SyntaxError) as e:
            print(f"{image} seems to be corrupted! ({type(e)})")
            return False  # TODO Delete?
            # raise e
        dircts = sanitize_path(image.absolute().relative_to(source_dir.absolute()))
        """The relative path of the destination image"""
        # library.store_image_filename(image)  # Store the file name in the known file names in the database.
        # This line was commented out to avoid storing moved files in the database
        if library.hash_in_hashes(imhash):
            if args.delete_action == 'nodelete':
                # Skip the file
                print(f"{image} was found in known hashes and will be skipped.")
                if not args.copy_files or args.found_action != "move":  # If the source image is not moved nor deleted, store the hash
                    library.store_image(image, imhash)
                return False
            elif args.delete_action == 'delete':
                print(f"{image} was found to be a duplicate and will be moved to trash.")
                delete(image)
                return False
            elif args.delete_action == 'move':
                new_image_path: Path = new_filename(duplicate_dir / dircts, args.limit_len, dircts.with_suffix('').name)
                print(f"{image} was found to be a duplicate. Moving to {new_image_path}")
                new_image_path.parent.mkdir(exist_ok=True, parents=True)
                shutil.move(image, new_image_path)
                library.store_image(image, imhash)
                library.store_image(new_image_path, imhash)
                return False
        library.store_image_hash(imhash)
        if not args.copy_files:
            library.store_image(image, imhash)  # Store original path in database
            print(f"Stored {image} into the database of known hashes.")
            return True
        dest = dest_dir / dircts
        if args.rename:
            dest = new_filename(dest, args.limit_len, imhash)  # Name after PHash
        else:
            dest = new_filename(dest, args.limit_len, dest.with_suffix('').name)  # Keep original name and truncate
        dest.parent.mkdir(exist_ok=True, parents=True)
        if args.found_action == "copy":
            print(f"Copying {image} to {dest}")
            shutil.copy2(image, dest)
        elif args.found_action == "move":
            print(
                f"Moving {image} to {dest}")
            # assert not os.path.isdir(dest), f"Destination {dest} was a dir!"
            # assert dest.parent.is_dir(), f"Directory {dest.parent} did not exist!"
            # assert os.path.isdir(dest.parent), f"Something strange happened with {str(dest.parent)}"
            shutil.move(image, dest)
        library.store_image(dest, imhash, image)  # Store new path in database
        return True


def main() -> int:
    """
    Main routine
    :return: 0 if successful, 1 otherwise
    """
    parser = argparse.ArgumentParser(prog=__file__,
                                     description='ImageHashSort - A Python Script to discard duplicate images in a huge '
                                                 'database of images, ignoring metadata.')
    parser.add_argument('--hash-only', required=False, action="store_const", const=False, default=True,
                        dest='copy_files',
                        help='If given, only hash the files found in the source directory, but do not modify them or copy anything.')
    parser.add_argument('--rename', required=False, action="store_const", const=False, default=False,
                        dest='rename',
                        help='If given, files will be renamed after their PHashes. '
                             'If a file name limit is given, file names will be truncated, even if this argument is not given.')
    parser.add_argument('--copy', required=False, action='store_const', const="copy", default="move",
                        dest='found_action',
                        help='If given, copy the files that are not already in the database instead of moving them')
    parser.add_argument('--delete', required=False, action='store_const', const="delete", default="nodelete",
                        dest='delete_action',
                        help='If given, delete all files that have been determined as duplicate from the source directory. '
                             'If combined with --hash-only, delete duplicates in-place. '
                             'Deleted files will be moved to the system trash. '
                             'If a file cannot be moved to trash, because its name is too long, it will be deleted '
                             'permanently, so use this argument with care!')
    parser.add_argument('--move-duplicates-to', required=False, action="store", dest="move_duplicates_dir", default=None,
                        help='Move duplicates to the given directory. Cannot be combined with the --delete argument.')
    parser.add_argument('--depth-first', required=False, action='store_const', const="depth-first", default="breadth-first",
                        dest='search_mode',
                        help='If given, perform a depth-first search instead of a breadth-first search. '
                             'This will favor files in subfolders of the directory structure over files in the root folder.'
                             ' The default search method is breadth-first search.')
    parser.add_argument('--source-dir', required=True, action="store", dest="source_dir",
                        help='Specify the source directory that contains the source images')
    parser.add_argument('--dest-dir', required=False, action="store", dest="dest_dir", default='out',
                        help='Specify the destination directory to move the source files into. Default is "out/"')
    parser.add_argument('--library', required=False, action="store", dest="lib_file", default='imagehashsort.db',
                        help='Specify the metadata library file. Default is "./imagehashsort.db"')
    parser.add_argument('--limit-filename-length', required=False, action="store_const", dest="limit_len", const=64,
                        default=-1, help='If given, limit the length of each output file name to 64 characters.')
    args = parser.parse_args()

    # Init Source Dir
    source_dir: Path = Path(args.source_dir)
    if not source_dir.is_dir():
        print(f"The source directory {source_dir} does not exist!", file=sys.stderr)
        return 1

    # Load the database from disk, if it exists - else, create a new one at the specified location
    library_file: Path = Path(args.lib_file)
    if library_file.is_file():
        library: ImageDatabase = JSONImageDatabase.load(library_file)
    else:
        library: ImageDatabase = JSONImageDatabase(library_file)

    dest_dir: Optional[Path] = None
    if args.copy_files:
        dest_dir: Path = Path(args.dest_dir)

    duplicate_dir: Optional[Path] = None
    if args.delete_action == 'delete':
        assert args.move_duplicates_dir is None, f"Error: --delete and --move-duplicates-to were both given"
    if args.move_duplicates_dir is not None:
        args.delete_action = 'move'
        duplicate_dir = Path(args.move_duplicates_dir)
        duplicate_dir.mkdir(exist_ok=True)

    try:
        # Main File Iteration
        dirs = deque()
        dirs.append(source_dir)
        copied: int = 1  # Avoid immediate autosave by initializing to 1
        if args.search_mode == 'breadth-first':
            while dirs:  # Do a breadth-first search through the directory structure to favor files in the root folder
                for child_path in dirs.popleft().iterdir():
                    if child_path.is_symlink():
                        continue  # Skip Links
                    if child_path.is_dir():
                        dirs.append(child_path)
                    elif child_path.is_file() and (not child_path.is_symlink()):
                        if is_picture(child_path):
                            # Process the image
                            if process_image(child_path, library, args, source_dir, dest_dir, duplicate_dir):
                                copied += 1
                                if copied % autosave_interval == 0:
                                    library.save()
        elif args.search_mode == 'depth-first':
            files = deque()
            while dirs:  # Do a depth-first search through the directory structure to favor files in subfolders
                d = dirs.popleft()
                if d.is_symlink():
                    continue  # Skip Links
                if d.is_dir():
                    nfiles, ndirs = [], []
                    for child_path in d.iterdir():
                        if child_path.is_dir():
                            ndirs.append(child_path)
                        elif child_path.is_file():
                            nfiles.append(child_path)
                    # Add dirs before files to guarantee kind-of depth-first search
                    dirs.extendleft(reversed(ndirs))
                    files.extendleft(reversed(nfiles))
            while files:
                d = files.popleft()
                if d.is_file() and (not d.is_symlink()):
                    if is_picture(d):
                        # Process the image
                        if process_image(d, library, args, source_dir, dest_dir, duplicate_dir):
                            copied += 1
                            if copied % autosave_interval == 0:
                                library.save()
    except KeyboardInterrupt:
        print(f"Process interrupted by user.")
    except BaseException as e:
        library.emergency_save()
        raise e

    library.save()

    return 0


if __name__ == "__main__":
    sys.exit(main())
