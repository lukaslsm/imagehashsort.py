"""
MIT License

Copyright (c) 2020-2022 Lukas Kirschner

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

JSONImageDatabase.py - Implementation of ImageDatabase that uses plain JSON files for storage
"""
from __future__ import annotations

import json
import shutil
import tempfile
from pathlib import Path
from typing import Any, Optional

from imagehashsort import ImageDatabase, DelayedKeyboardInterrupt


class JSONImageDatabase(ImageDatabase):
    """
    This class represents an image database that can be saved and loaded from plain JSON files.
    """
    k_imagehash: str = "phash"
    """JSON key for the Image Hash"""
    k_originalpath: str = "origin"
    """JSON Key for the original path"""

    def __init__(self, file: Path, *args, **kwargs):
        super().__init__(name="JSON Database", *args, **kwargs)
        self.file: Path = file
        """The path where the database file should be stored"""
        self.known_hashes: set[str] = set()
        """The set of all already-known perceptual image hashes"""
        self.known_filenames: set[str] = set()
        """The set of all already-known file names"""
        self.library: dict[str, dict[Any, Any]] = dict()
        """The library that stores all the image information"""

    def emergency_save(self) -> None:
        try:
            bak_file = self.file.parent / f"{self.file.name}.emergency_backup"
            if self.file.is_file() and not bak_file.is_file():
                super().log(f"An exception occurred. Backing up database to {bak_file} before attempting to save.\n"
                            f"Please manually investigate and delete the backup file if it is not needed anymore!")
                with DelayedKeyboardInterrupt():
                    shutil.copy2(self.file, bak_file)
            self.save()
        except BaseException as e:
            tdir = tempfile.gettempdir()
            tfile = Path(tdir) / "emergency_database_backup.json"
            print(
                f"Could not emergency-save database! Trying to create a copy at {tfile} - Please manually copy the Backup to the Database Location!"
                f" If even this operation fails, the database content will be discarded.")
            self.save(tfile)
            raise e

    def save(self, file: Optional[Path] = None) -> None:
        if file is None:
            file = self.file
        with DelayedKeyboardInterrupt():
            backup_database: Path = file.parent / f"{file.name}.bak"
            super().log(f"Saving library to {file}, copying the old database to {backup_database} in case of crash ...")
            if file.is_file():
                shutil.copy2(file, backup_database)
            with file.open("w") as lib:
                json.dump(self.library, lib, indent=1)

    def save_as(self, new_path: Path) -> ImageDatabase:
        new_database: JSONImageDatabase = JSONImageDatabase(new_path)
        new_database.library = self.library
        new_database.known_filenames = self.known_filenames
        new_database.known_hashes = self.known_hashes
        new_database.save()
        return new_database

    @staticmethod
    def load(file: Path) -> JSONImageDatabase:
        new_database = JSONImageDatabase(file)

        # Init Library
        if not new_database.file.is_file():
            raise FileNotFoundError(f"Database file not found at {file}")
        new_database.log(f"Loading existing library from {new_database.file.as_posix()} ...")
        with new_database.file.open("r") as lib:
            new_database.library = json.load(lib)
        new_database.log(f"Loading existing image hashes...")
        for path, entry in new_database.library.items():
            if JSONImageDatabase.k_imagehash in entry:
                new_database.known_hashes.add(entry[JSONImageDatabase.k_imagehash])
            if JSONImageDatabase.k_originalpath in entry:
                new_database.known_filenames.add(entry[JSONImageDatabase.k_originalpath])
            new_database.known_filenames.add(path)
        return new_database

    def image_in_filenames(self, image: Path) -> bool:
        query_path: Path = super()._resolve_path(image, self.file)
        return str(query_path) in self.known_filenames

    def store_image_filename(self, image: Path) -> None:
        query_path: Path = super()._resolve_path(image, self.file)
        query_str: str = str(query_path)
        self.known_filenames.add(query_str)
        # For the JSONImageDatabase, this function should also store a new entry in the library, if not available
        # This allows persistence of those records on re-load
        if query_str not in self.library:
            self.library[query_str] = dict()

    def hash_in_hashes(self, imhash: str) -> bool:
        return imhash in self.known_hashes

    def store_image(self, image: Path, imhash: str, original_path: Optional[Path] = None, *args, **kwargs) -> None:
        query_path: Path = super()._resolve_path(image, self.file)
        query_str: str = str(query_path)
        self.store_image_filename(image)
        assert query_str in self.library
        self.store_image_hash(imhash)
        self.library[query_str][JSONImageDatabase.k_imagehash] = imhash
        if original_path is not None:
            or_path: str = str(super()._resolve_path(original_path, self.file))
            self.library[query_str][JSONImageDatabase.k_originalpath] = or_path
            self.known_filenames.add(or_path)

    def store_image_hash(self, imhash: str) -> None:
        self.known_hashes.add(imhash)
