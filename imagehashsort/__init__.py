"""
MIT License

Copyright (c) 2020-2022 Lukas Kirschner

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

imagehashsort - This is the base package for imagehashsort.py
"""
from imagehashsort.DelayedKeyboardInterrupt import DelayedKeyboardInterrupt
from imagehashsort.Hash import perceptual_hash
from imagehashsort.ImageDatabase import ImageDatabase
from imagehashsort.JSONImageDatabase import JSONImageDatabase

known_extensions: set[str] = set(x.casefold() for x in {
    "jpg",
    "jpeg",
    "png",
    "tif",
    "tiff",
    "bmp",
    # "gif",
    "tga"
})
"""All known and supported image file extensions in lower-case"""
