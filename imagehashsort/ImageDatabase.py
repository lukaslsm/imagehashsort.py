"""
MIT License

Copyright (c) 2020-2022 Lukas Kirschner

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

ImageDatabase.py - Abstract Base Class for the ImageDatabase
"""
from __future__ import annotations

from abc import ABCMeta, abstractmethod
from pathlib import Path
from typing import Optional


class ImageDatabase(metaclass=ABCMeta):
    """This class represents the base class of an ImageDatabase, that should provide a way to store image metadata."""

    def __init__(self, name: str, *args, **kwargs):
        """
        Instantiate a new Image Database

        :param name: Human-readable name
        :param args: More positional arguments
        :param kwargs: More keyword arguments
        """
        super().__init__()
        self.name: str = name
        """A human-readable name of this image database, e.g. "My new image database"."""

    @abstractmethod
    def emergency_save(self) -> None:
        """
        Emergency-save this database.
        This may happen if an uncaught exception occurs.
        Calling this method should try to save the image database, doing a backup of an older version of the database.

        :return: None
        """
        pass

    @abstractmethod
    def save(self) -> None:
        """
        Save this database normally.
        This will overwrite any existing databases

        :return: None
        """

    @abstractmethod
    def save_as(self, new_path: Path) -> ImageDatabase:
        """
        Save this database to the specified location, leaving the old database file untouched.
        This operation moves the memory content of the old database to the new database, invalidating the old database
        (this instance, where save_as() has been called)

        :param new_path: New path to save the database file to
        :return: the database that represents the newly-saved database at the new location
        """
        pass

    @property
    def readable_name(self) -> str:
        """
        Get a human-readable name of this database

        :return: the human-readable name of this database, e.g. "My new image database".
        """
        return self.name

    # noinspection PyMethodMayBeStatic
    def log(self, message: str) -> None:
        """
        Log a message in the name of this database
        :param message: Message to log
        :return: None
        """
        print(message)

    @staticmethod
    def _resolve_path(path: Path, database_path: Path):
        """
        Resolve the given path in the following way:
            1. If the parent of the database file is a parent of the given path, return the path relative to the database file's parent
            2. If this is not the case, return the full absolute path of the given file.

        :param path: Path to resolve
        :param database_path: Path of the database
        :return: the resolved path
        """
        parent_path: Path = database_path.absolute().parent
        try:
            return path.absolute().relative_to(parent_path)
        except ValueError:
            return path.absolute()

    @abstractmethod
    def image_in_filenames(self, image: Path) -> bool:
        """
        Check if the given file name is already in the database.
        All file names will be automatically resolved relative to the location of the image database, or absolute,
            if the image database's parent is not a parent of the given path.
        :param image: Image file to resolve
        :return: True, if the image file is already known
        """
        pass

    @abstractmethod
    def hash_in_hashes(self, imhash: str) -> bool:
        """
        Check if the given hash is already in the database.

        :param imhash: Hash to query
        :return: True, if the image hash is already known
        """
        pass

    @abstractmethod
    def store_image_filename(self, image: Path) -> None:
        """
        Store the given image file into the database.

        :return: None
        """
        pass

    @abstractmethod
    def store_image_hash(self, imhash: str) -> None:
        """
        Store the given image hash into the database.
        Caution - This might not actually save the hash, i.e. the hash might not survive a re-load of the database!
        To save the hash persistently, use store_image()

        :param imhash: The hash to save
        :return: None
        """
        pass

    @abstractmethod
    def store_image(self, image: Path, imhash: str, original_path: Optional[Path] = None, *args, **kwargs) -> None:
        """
        Store the given image and all given information in the database

        :param image: Full path of the image
        :param imhash: Hash of the image
        :param original_path: The original path of the image, where the image originated from (e.g. if it was copied from a source folder)
        :param args: More args
        :param kwargs: More keyword args that will be stored in the database
        :return: None
        """
        pass
