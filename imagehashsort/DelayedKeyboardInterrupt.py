import signal


class DelayedKeyboardInterrupt(object):
    """
    This class provides a ContextManager to delay keyboard interrupts in a certain block of code.
    Borrowed from https://stackoverflow.com/a/21919644/8390457
    """

    def __enter__(self):
        self.signal_received = False
        self.old_handler = signal.signal(signal.SIGINT, self.handler)

    def handler(self, sig, frame):
        self.signal_received = (sig, frame)

    def __exit__(self, type, value, traceback):
        signal.signal(signal.SIGINT, self.old_handler)
        if self.signal_received:
            self.old_handler(*self.signal_received)
