# imagehashsort.py

A python library for keeping track of the content of a large image library.
With this library, duplicate images can easily be discarded, existing libraries can be catalogued and new images can automatically be sorted into an existing library, based on their perceptual image hash.
Designed to be scalable to huge image libraries of over 100k images.

This library requires Python 3.10 and the following Python packages:

* `PIL`
* `send2trash`
* `imagehash`

imagehashsort organizes images in a *library*, keeping track of the location, the perceptual image hash and other metadata.

The included stand-alone program `imagehashsort` can perform most standard tasks like

* Creating a new library file based on an existing set of images
* Copying or moving new images into an existing directory structure
* Deleting or moving duplicate images from an existing library

The directory tree can be traversed either using a breadth-first or depth-first search, which is useful to prioritize images nested in deeper subfolders, e.g. for scraped imgur albums.
With breadth-first search, images are favored that are stored higher up in the tree, i.e. if a duplicate image is stored in the root folder and a subfolder, the image in the root folder is kept and the image in the subfolder is discarded.

Warning - Images are always stored into the database either relative to the database, if the database's parent folder is in one of the images parent folders, or absolute, if this is not the case!
Be sure to either have the database at a different location than the images, or move the database together with the images, if you need to move it.

## Installation

The program can be installed using `pip` by typing `pip3 install "git+git@gitlab.com:lukaslsm/imagehashsort.py.git"`.
Another way is to clone this repository and installing into a venv using `pip3 install -e /dir/to/cloned/repo`

## Example Usage

### Delete duplicate images in-place

This will create a database of hashes at `database.json` and delete all duplicate files from the `images/` directory:

```bash
python3 imagehashsort.py --library database.json --source-dir images --hash-only --delete
```

### Move duplicate images to a duplicates folder and prioritize files that are nested deeper in the directory structure

This will create a database of hashes at `database.json` and move all duplicate files from the `images/` directory into a `duplicates/` directory.
Files further nested are favored during the process (i.e. a depth-first search instead of a breadth-first search is performed on the directory structure):

```bash
python3 imagehashsort.py --library database.json --source-dir images --hash-only --depth-first --move-duplicates-to duplicates
```

### Copy all non-duplicate files onto an eCryptFS volume

This operation will copy all non-duplicate files from the `images/` folder onto an eCryptFS volume, limiting the file names on the target drive to 128 characters.
The hash database itself is stored on the eCryptFS volume in `database.json`.

```bash
python3 imagehashsort.py --library /mnt/crypt/database.json --source-dir images --dest-dir /mnt/crypt/images --copy --limit-filename-length 128
```

# Library Documentation

## Example usage

A usage example for the imagehashsort library can be seen in [imagehashsort.py](imagehashsort.py) where the stand-alone application is implemented.

## Image Databases

The following class diagram describes the `ImageDatabase` base class and all its members:

```plantuml
@startuml
allowmixing

abstract class   ImageDatabase
class   JSONImageDatabase

ImageDatabase <|-- JSONImageDatabase

ImageDatabase : -name: str
ImageDatabase : +printable_name: str
ImageDatabase : +log(): str
ImageDatabase : -_resolve_path(Path,Path): Path
ImageDatabase : {abstract} +emergency_save(): None
ImageDatabase : {abstract} +save(): None
ImageDatabase : {abstract} +save_as(): None
ImageDatabase : {abstract} +readable_name(): None
ImageDatabase : {abstract} +image_in_filenames(Path): bool
ImageDatabase : {abstract} +hash_in_hashes(str): bool
ImageDatabase : {abstract} +store_image_filename(Path): bool
ImageDatabase : {abstract} +store_image_hash(str): bool
ImageDatabase : {abstract} +store_image(Path,str,Path): bool

JSONImageDatabase : +emergency_save(): None
JSONImageDatabase : +save(): None
JSONImageDatabase : +save_as(): None
JSONImageDatabase : +readable_name(): None
JSONImageDatabase : +image_in_filenames(Path): bool
JSONImageDatabase : +hash_in_hashes(str): bool
JSONImageDatabase : +store_image_filename(Path): bool
JSONImageDatabase : +store_image_hash(str): bool
JSONImageDatabase : +store_image(Path,str,Path): bool

namespace "JSON File" as jsonfile {
    note "Dict of file paths to hashes\nand other metadata" as n1
}

JSONImageDatabase <-r-> jsonfile : read/write

@enduml
```

# Version History

| Date       | Version | Changelog                                                                                                                      |
|------------|---------|--------------------------------------------------------------------------------------------------------------------------------|
| 2022-09-08 | v0.1    | Refactored the old `imagehashsort.py` to be extensible, object-oriented and to support pip. Implemented a JSON-based database. |